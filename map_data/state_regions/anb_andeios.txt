﻿STATE_SOUTHERN_KHEIONS = {
    id = 700
    subsistence_building = "building_subsistence_farms"
    provinces = { "x597C52" "x599BFE" "x7A3061" "x863569" "x916505" "x97BEC2" "xB2428C" "xD18BB9" "xD52654" }
    traits = { "state_trait_kaydhano" "state_trait_kherka_mineral_fields" }
    city = "x863569" #Lokemeion
    farm = "x97bec2" #Kherka
    wood = "xb2428c" #Agoxetei
    port = "xd18bb9" #Sothan
    mine = "x599bfe" #Coskheis
    arable_land = 35
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_vineyard_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 17
        bg_fishing = 11
        bg_lead_mining = 20
        bg_iron_mining = 48
        bg_coal_mining = 60
        bg_gem_mining = 2
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 2
    }
    naval_exit_id = 3127
}
STATE_CENTRAL_KHEIONS = {
    id = 702
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1AD874" "x2D6B28" "x3A937C" "x476491" "x528080" "x7C3800" "x92427E" "x9853BA" "xAA537C" "xBA8B00" "xC15B1F" "xC64A69" "xCC4F62" "xFDFF9E" "xFF653F" }
    traits = { "state_trait_kaydhano" "state_trait_natural_harbors" }
    city = "xfdff9e" #Degakheion
    farm = "x476491" #Agholor
    wood = "x3a937c" #Promethe
    port = "xba8b00" #Ormam
    mine = "xff653f" #Astolion
    arable_land = 30
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_vineyard_plantations bg_tobacco_plantations bg_silk_plantations }
    capped_resources = {
        bg_logging = 9
        bg_fishing = 13
        bg_lead_mining = 27
        bg_iron_mining = 20
    }
    naval_exit_id = 3127
}
STATE_NORTIKAN = {
    id = 703
    subsistence_building = "building_subsistence_farms"
    provinces = { "x4408B3" "x485023" "x5664FF" "x5665FF" "x7ED82E" "xBA2EB0" "xE723B3" "xFF9999" }
    traits = { "state_trait_kaydhano" "state_trait_nortikan_mine" }
    port = "xe723b3" #Urargora
    city = "xff9999" #Tirberana
    farm = "xba2eb0" #Sarkiona
    wood = "x5665ff" #Sghenaorre
    mine = "x4408b3" #Tinideoro
    arable_land = 25
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_sugar_plantations bg_vineyard_plantations bg_dye_plantations }
    capped_resources = {
        bg_logging = 9
        bg_fishing = 13
        bg_lead_mining = 27
        bg_iron_mining = 20
    }
    naval_exit_id = 3128
}
STATE_NORTHERN_KHEIONS = {
    id = 704
    subsistence_building = "building_subsistence_farms"
    provinces = { "x26686D" "x2EBA92" "x3094DA" "x35FF4D" "x5D6B25" "x74A674" "x78376F" "x862C7C" "x93781F" "xA638CE" "xFF8D47" }
    traits = { "state_trait_kaydhano" }
    city = "x35ff4d" #Oktikheion
    farm = "xff8d47" #Dhanopadi
    wood = "x3094da" #Melnar
    port = "x5d6b25" #Arpedifer
    mine = "x2eba92" #Mtelian
    arable_land = 30
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_sugar_plantations bg_vineyard_plantations bg_cotton_plantations bg_tobacco_plantations bg_dye_plantations bg_silk_plantations }
    capped_resources = {
        bg_logging = 2
        bg_fishing = 10
        bg_lead_mining = 12
        bg_gold_mining = 2
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 2
    }
    naval_exit_id = 3127
}
STATE_SOUTIKAN = {
    id = 705
    subsistence_building = "building_subsistence_farms"
    provinces = { "x03C480" "x44E6B3" "x502080" "xB7FF8E" "xBC4343" "xCE982D" "xDBAE5C" }
    traits = { "state_trait_kaydhano" }
    city = "x03c480" #Alosthana
    farm = "xbc4343" #Gonomoro
    wood = "xdbae5c" #Saluistighe
    port = "x44e6b3" #Urartola
    mine = "xce982d" #Iembartiana
    arable_land = 50
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_vineyard_plantations bg_tobacco_plantations bg_dye_plantations }
    capped_resources = {
        bg_logging = 12
        bg_fishing = 9
        bg_whaling = 4
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 30
    }
    naval_exit_id = 3128
}
STATE_VOTHELISI_ARPENISI = {
    id = 706
    subsistence_building = "building_subsistence_orchards"
    provinces = { "x2052A3" "x52CEAB" "x5DBC54" "x778011" "x7C3F3E" "x83C677" "xB2C649" "xDDD468" }
    traits = { "state_trait_kaydhano" }
    city = "x778011" #Serigastisi
    farm = "x83c677" #Somelisi
    wood = "x52ceab" #Vothelisi
    port = "x7c3f3e" #Anisigo
    mine = "xb2c649" #Cosan
    arable_land = 20
    arable_resources = { bg_wheat_farms bg_sugar_plantations bg_tobacco_plantations bg_vineyard_plantations bg_dye_plantations }
    capped_resources = {
        bg_whaling = 3
        bg_fishing = 14
        bg_gem_mining = 1
        bg_logging = 3
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 5
    }
    naval_exit_id = 3127
}
STATE_ANISIKHEION = {
    id = 707
    subsistence_building = "building_subsistence_orchards"
    provinces = { "x4C769B" "x89FF7C" "xC9DB2B" "xEAE36B" "xF1CF81" }
    traits = {}
    city = "xc9db2b" #Anisikheion
    farm = "xf1cf81" #Agonisi
    wood = "xeae36b" #Anikhein
    port = "x89ff7c" #Anisoton
    mine = "x4c769b" #Proylasi
    arable_land = 30
    arable_resources = { bg_wheat_farms bg_tobacco_plantations bg_vineyard_plantations bg_sugar_plantations }
    capped_resources = {
        bg_whaling = 4
        bg_fishing = 15
        bg_gem_mining = 2
        bg_logging = 4
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 5
    }
    naval_exit_id = 3127
}
STATE_EMPKEIOS = {
    id = 708
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0B335F" "x21C698" "x4A0FF6" "x6F1F5F" "x7183C4" "x891C2E" "x96E7C4" "xC81FC4" "xEF1291" }
    traits = { "state_trait_agotham_river" "state_trait_natural_harbors" }
    city = "x891c2e" #Dhanam
    farm = "x96e7c4" #Vorlidir
    wood = "x6F1F5F" #Serigso
    port = "x7183c4" #Empkeios
    mine = "x4a0ff6" #Rinigos
    arable_land = 65
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_sugar_plantations bg_tobacco_plantations bg_dye_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_logging = 3
        bg_fishing = 10
    }
    naval_exit_id = 3127
}
STATE_KEYOLION = {
    id = 709
    subsistence_building = "building_subsistence_farms"
    provinces = { "x6F3D5F" "x7F96FF" "xC466C4" "xED1F2E" "xEDE72E" "xEDE73E" "xFF59B9" "xFFE45E" }
    traits = { "state_trait_waenofah_mine" }
    city = "x6f3d5f" #Poroga
    farm = "xede73e" #Lokeyogen
    wood = "xffe45e" #Phineion
    port = "xed1f2e" #Keyolion
    mine = "x7f96ff" #Eionetei
    arable_land = 55
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_tobacco_plantations bg_vineyard_plantations bg_sugar_plantations bg_cotton_plantations bg_dye_plantations }
    capped_resources = {
        bg_logging = 7
        bg_fishing = 5
        bg_iron_mining = 60
        bg_sulfur_mining = 28
    }
    naval_exit_id = 3127
}
STATE_ENEION = {
    id = 710
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1D2677" "x33FF28" "x3CD639" "x57CC59" "x698777" "x844C6C" "xAF4190" "xC0652E" "xEA5656" "xFF6B1C" }
    traits = {}
    city = "xff6b1c" #Trilidir
    farm = "xea5656" #Eltanedio
    wood = "x698777" #Lartor 
    port = "x3cd639" #Eneion
    mine = "x57cc59" #Tritheiam
    arable_land = 70
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_tobacco_plantations bg_vineyard_plantations bg_cotton_plantations bg_sugar_plantations bg_dye_plantations }
    capped_resources = {
        bg_logging = 9
        bg_fishing = 7
        bg_lead_mining = 12
        bg_iron_mining = 18
        bg_sulfur_mining = 23
    }
    resource = {
        type = "bg_gem_mining"
        undiscovered_amount = 2
    }
    naval_exit_id = 3127
}
STATE_BESOLAKI = {
    id = 711
    subsistence_building = "building_subsistence_farms"
    provinces = { "x30377F" "x6ABA50" "x83B76F" "x9692B8" "xC5FF35" "xD67183" "xFF3D8A" }
    traits = {}
    city = "xc5ff35" #Divaigho
    farm = "xd67183" #Nakothein
    wood = "xff3d8a" #Basorekos
    port = "x30377f" #Besolaki
    mine = "x9692b8" #Akrexyl
    arable_land = 45
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_tobacco_plantations bg_vineyard_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 12
        bg_fishing = 15
        bg_lead_mining = 25
        bg_sulfur_mining = 9
    }
    naval_exit_id = 3127
}
STATE_AMGREMOS = {
    id = 712
    subsistence_building = "building_subsistence_farms"
    provinces = { "x2CD638" "x4F827D" "x68105B" "x82401D" "x88D294" "xA57475" "xD47392" }
    traits = { "state_trait_andic_woodlands" }
    city = "x68105B" #Thoreleion
    farm = "x82401D" #Vexyeltokti
    wood = "x88D294" #Eltibas
    port = "x4F827D" #Amgremos
    mine = "x2CD638" #Serigthan
    arable_land = 45
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_tobacco_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_logging = 26
        bg_fishing = 12
        bg_whaling = 4
        bg_sulfur_mining = 21
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 10
    }
    naval_exit_id = 3129
}
STATE_APIKHOXI = {
    id = 713
    subsistence_building = "building_subsistence_farms"
    provinces = { "x157568" "x378247" "x41DB58" "x5C3BEF" "x689E52" "x6B4264" "xA3463A" "xA3CF3A" "xB52D51" "xDBD3A4" "xFF4B42" }
    traits = { "state_trait_xiktheam_river" }
    port = "x6b4264" #Gapoueion
    city = "x378247" #Apikhoxi
    farm = "x41db58" #Koineion
    wood = "xa3463a" #Eirideion
    mine = "xdbd3a4" #Kherxel
    arable_land = 125
    arable_resources = { bg_cotton_plantations bg_tobacco_plantations bg_maize_farms bg_livestock_ranches bg_vineyard_plantations bg_sugar_plantations bg_dye_plantations }
    capped_resources = {
        bg_logging = 5
        bg_fishing = 6
    }
    naval_exit_id = 3127
}
STATE_VOLITHORAM = {
    id = 750
    subsistence_building = "building_subsistence_farms"
    provinces = { "x290F5F" "x6836F6" "x825582" "xC66253" "xD83A22" "xD843C9" "xEDE626" }
    traits = { "state_trait_andic_woodlands" }
    city = "x290f5f" #Volithoram
    farm = "xd83a22" #Astogelim
    wood = "xc66253" #Kheinsoba
    mine = "x6836f6" #Eiongremos
    arable_land = 50
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_tobacco_plantations bg_vineyard_plantations bg_cotton_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 20
        bg_coal_mining = 34
        bg_sulfur_mining = 17
    }
}
STATE_OKTIAMOTON = {
    id = 751
    subsistence_building = "building_subsistence_farms"
    provinces = { "x3E2D91" "x5377C4" "x651F5F" "xD1DB91" "xD8319E" "xF01FC4" }
    traits = { "state_trait_andic_woodlands" }
    city = "x5377c4" #Oktiamoton
    farm = "x3e2d91" #Padilor
    port = "x651f5f" #Sotiredion
    wood = "xd8319e" #Lokexyl
    mine = "xd1db91" #Vexylamothon
    arable_land = 60
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_tobacco_plantations bg_vineyard_plantations bg_cotton_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 17
        bg_fishing = 11
        bg_whaling = 2
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 20
    }
    naval_exit_id = 3129
}
STATE_EASTERN_MTEIBHARA = {
    id = 737
    subsistence_building = "building_subsistence_farms"
    provinces = { "x12CAFC" "x14832E" "x2BDDFB" "x301FC4" "x3B84F6" "x84102E" "x991F5F" "xB9E3C4" "xC31FC4" "xDECBEF" "xE3C92E" "xF8F85F" }
    traits = { "state_trait_mteibas_valley" "state_trait_ameionam_river" "state_trait_mteibhara_coal_mine" }
    city = "xC31FC4" #Jarrelaban
    farm = "xF8F85F" #Dalktash
    wood = "xB9E3C4" #Pasrazien
    mine = "x301FC4" #Hfaesban
    arable_land = 50
    arable_resources = { bg_cotton_plantations bg_maize_farms bg_livestock_ranches bg_tobacco_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_logging = 22
        bg_iron_mining = 34
        bg_coal_mining = 45
    }
}
STATE_WESTERN_MTEIBHARA = {
    id = 714
    subsistence_building = "building_subsistence_farms"
    provinces = { "x031FC4" "x060F5F" "x420D5F" "x451FF6" "x5A1F2E" "x687CF6" "x77A2A6" "x811F91" "x9C27BD" "xA7252E" "xAEE791" "xC0C92E" "xD81FF6" "xE61FC4" "xECAB0F" }
    traits = { "state_trait_mteibas_valley" }
    city = "xd81ff6" #Mzhaara
    farm = "x811F91" #Rabsharay
    wood = "x451FF6" #Qashandah
    mine = "xC0C92E" #Wzhyldhol
    arable_land = 60
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_tobacco_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_logging = 18
        bg_lead_mining = 36
        bg_iron_mining = 24
        bg_coal_mining = 20
    }
}
STATE_DEYEION = {
    id = 715
    subsistence_building = "building_subsistence_farms"
    provinces = { "x262CC4" "x7D1F2E" "x9292F6" "xA40C91" "xBCBF5F" "xCE9AF6" "xFBBCF6" }
    traits = { "state_trait_andic_woodlands" }
    city = "x262CC4" #Astakhotein
    farm = "xCE9AF6" #Dhanokti
    wood = "xFBBCF6" #Divala
    mine = "x7D1F2E" #Koeal
    arable_land = 60
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_tea_plantations bg_tobacco_plantations bg_vineyard_plantations bg_cotton_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 9
        bg_fishing = 14
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 10
    }
    naval_exit_id = 3129
}
STATE_AMEION = {
    id = 716
    subsistence_building = "building_subsistence_farms"
    provinces = { "x118391" "x381F5F" "x501F2E" "x5C2D91" "x779AF6" "x7AE891" "x8F135F" "x9B1FC4" "xA183F6" "xB6C92E" "xF51FC4" }
    traits = { state_trait_ameionam_river state_trait_natural_harbors state_trait_andic_woodlands }
    city = "x1d29f6" #Haebysziund
    farm = "x8f135f" #Heztwanysar
    wood = "x501f2e" #Khrghaana
    port = "x118391" #Ameyban
    mine = "x779af6" #Tzkheban
    arable_land = 175
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_tobacco_plantations bg_vineyard_plantations bg_tea_plantations bg_cotton_plantations }
    capped_resources = {
        bg_logging = 24
        bg_fishing = 18
        bg_gold_mining = 1
        bg_lead_mining = 20
        bg_iron_mining = 30
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 30
    }
    naval_exit_id = 3129
}
STATE_EASTERN_CHENDHYA = {
    id = 717
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0879C4" "x23DD2E" "x47C25F" "x6E012E" "x861F91" "xCB295F" "xEC33F6" }
    traits = { "state_trait_chendhya_plains" }
    city = "x47c25f" #Nopanais
    farm = "xcb295f" #Budar Gaednae
    wood = "x23dd2e" #Xartraelas, But there is no Logging :/ so sad xartraelas will never be real unless i fuck up
    mine = "x6E012E" #Traerraden
    arable_land = 70
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_tobacco_plantations bg_cotton_plantations }
    #capped_resources = {
    #}
    resource = {
        type = "bg_damestear_fields"
        depleted_type = "bg_damestear_mining"
        undiscovered_amount = 3
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 5
    }
}
STATE_CENTRAL_CHENDHYA = {
    id = 730
    subsistence_building = "building_subsistence_farms"
    provinces = { "x2D832E" "x3583C4" "x4D8491" "x571F91" "x5F7A2E" "xAB1FF6" "xB3DB91" "xC5C52E" "xD5D55F" "xDA1A5F" }
    traits = { "state_trait_chendhya_plains" "state_trait_agotham_river" }
    city = "xDA1A5F" #Samidon
    farm = "xAB1FF6" #Bacaeuaes
    wood = "x571F91" #Faeziundzaern, But there is no Logging :/
    mine = "x2D832E" #Gaednaemoxbin
    arable_land = 65
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_tobacco_plantations bg_cotton_plantations }
    #capped_resources = {
    #}
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 10
    }
}
STATE_WESTERN_CHENDHYA = {
    id = 738
    subsistence_building = "building_subsistence_farms"
    provinces = { "x191FF6" "x204091" "x32832E" "x6C1FC4" "x9E725F" "xB0B5F6" "xDD90F6" "xEA1F91" }
    traits = { "state_trait_chendhya_plains" "state_trait_agotham_river" }
    city = "x191FF6" #Skynbharoga
    farm = "xDD90F6" #Pithion
    wood = "x6C1FC4" #Skynpedan
    mine = "xEA1F91" #Xegra
    arable_land = 75
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_tobacco_plantations bg_cotton_plantations }
    #capped_resources = {
    #}
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 10
    }
}
STATE_CLEMATAR = {
    id = 718
    subsistence_building = "building_subsistence_farms"
    provinces = { "x2A0C91" "x5483F6" "x696D2E" "x7E015F" "xA8975F" "xBDDD91" "xD20BC4" "xFC1F2E" "xFC832E" }
    traits = { state_trait_endioka_river }
    city = "x696d2e" #Lodhrim
    farm = "x7e015f" #Svirsi Vindar
    wood = "x05ca2e" #Pedhrim
    mine = "xfc832e" #Genhi Varamesh
    arable_land = 126
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations bg_tobacco_plantations bg_tea_plantations }
    capped_resources = {
        bg_logging = 6
        bg_coal_mining = 40
        bg_iron_mining = 18
    }
}
STATE_ENLARMAI = {
    id = 719
    subsistence_building = "building_subsistence_farms"
    provinces = { "x162091" "x2B33C4" "x43C991" "x7F1491" "x94A15F" "xBE64C4" "xD01B3B" "xD31FF6" }
    traits = { state_trait_endioka_river }
    city = "x2b33c4" #Enlarmai
    farm = "x43c991" #Sithra Calindash
    wood = "x8282c4" #Imarti Sadvya
    mine = "x4077f6" #Ethra
    arable_land = 116
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations bg_tobacco_plantations bg_tea_plantations }
    capped_resources = {
        bg_logging = 12
        bg_lead_mining = 45
    }
}
#STATE_LOKEMTRIA = {
#    id = 720
#    subsistence_building = "building_subsistence_farms"
#    provinces = {}
#    traits = {}
#    city = "x4c015f" #Varanya Sandaman
#
#    farm = "x957a91" #Dongyccyl
#
#    wood = "x0d1fc4" #Tayakorrim
#
#    port = "x8b0b91" #Korrimutren
#
#    mine = "x80b55f" #Enretvil
#
#    arable_land = 5 #placeholder
#    arable_resources = { bg_maize_farms bg_livestock_ranches bg_dye_plantations bg_tea_plantations bg_vineyard_plantations }
#    capped_resources = {
#        bg_logging = 1
#        bg_fishing = 1
#    }
#    
#}
STATE_IRON_HILLS = {
    id = 721
    subsistence_building = "building_subsistence_farms"
    provinces = { "x213741" "x3D1F5F" "x610C91" "x64032E" "x6715C4" "x7C83F6" "xA61FF6" "xD0C75F" "xE51F91" }
    traits = { state_trait_iron_hills }
    city = "xa61ff6" #Banderuttai
    farm = "x7c83f6" #Thekavasanikkunnu
    wood = "x6715c4" #Varaendi
    mine = "xd0c75f" #Kattisangamar
    arable_land = 100
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_dye_plantations bg_tea_plantations bg_cotton_plantations }
    capped_resources = {
        bg_logging = 17
        bg_coal_mining = 12
        bg_iron_mining = 45
    }
}
STATE_NANRU_NAKAR = {
    id = 722
    subsistence_building = "building_subsistence_farms"
    provinces = { "x19BF2E" "x1CABC4" "x283D2E" "x2EE75F" "x521F91" "x5826C4" "x5B025F" "x6D29F6" "x91FFC4" "x97A1F6" "x9AF391" "xACBF2E" "xC18B5F" "xCFCA7A" "xD6E791" }
    traits = { state_trait_kalavend_river }
    city = "x2ee75f" #Nakar
    farm = "x6d29f6" #Varamkq
    wood = "x19bf2e" #Kattvyavaram
    mine = "x283d2e" #Uesrenti
    arable_land = 243
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations bg_dye_plantations bg_silk_plantations bg_tea_plantations }
    capped_resources = {
        bg_logging = 11
        bg_coal_mining = 72
    }
}
STATE_NEOR_EMPKEIOS = {
    id = 723
    subsistence_building = "building_subsistence_farms"
    provinces = { "x34B791" "x37832E" "x738D2E" "xB2035F" "xB5C9F6" "xDC2CC4" "xDF845F" "xF165F6" }
    traits = { state_trait_empkeiosam_river state_trait_natural_harbors }
    city = "xb2035f" #Andital
    farm = "xf165f6" #Anakalchend
    wood = "x37832e" #Telinkad
    port = "x34b791" #Nagar Vyechei
    mine = "x738d2e" #Varanyachend
    arable_land = 172
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_dye_plantations bg_tea_plantations bg_vineyard_plantations bg_cotton_plantations }
    capped_resources = {
        bg_logging = 23
        bg_fishing = 13
    }
    resource = {
        type = "bg_damestear_fields"
        depleted_type = "bg_damestear_mining"
        undiscovered_amount = 3
    }
    naval_exit_id = 3129
}
STATE_KLERECHEND = {
    id = 724
    subsistence_building = "building_subsistence_farms"
    provinces = { "x028391" "x0D1FC4" "x227AF6" "x41852E" "x4C015F" "x77BF55" "x8B0B91" "xA0DDC4" "xC926AD" "xCACA2E" "xFA8DC4" }
    traits = { state_trait_noriam_river }
    city = "xa3bf5f" #Sthanan it Vussam
    farm = "xcaca2e" #Kannalulthe
    wood = "x3a01c4" #Bayasidaphoriha
    mine = "x610c91" #Gophira
    arable_land = 133
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_dye_plantations bg_tea_plantations bg_vineyard_plantations bg_cotton_plantations }
    capped_resources = {
        bg_logging = 14
        bg_fishing = 12
        bg_sulfur_mining = 40
    }
    naval_exit_id = 3129
}
STATE_NYMBHAVA = {
    id = 725
    subsistence_building = "building_subsistence_farms"
    provinces = { "x250691" "x3A01C4" "x79A15F" "x9E6506" "xA3BF5F" "xB81F91" "xCD01C4" "xE229F6" "xF7832E" }
    traits = { state_trait_empkeiosam_river state_trait_good_soils }
    city = "xe229f6" #Nymbhava
    farm = "x64032e" #Uddachend
    wood = "xcd01c4" #Sibisimra
    mine = "x250691" #Matnadevela
    arable_land = 147
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations bg_dye_plantations bg_tea_plantations }
    capped_resources = {
        bg_logging = 20
        bg_coal_mining = 32
    }
}
STATE_DEGITHION = {
    id = 726
    subsistence_building = "building_subsistence_farms"
    provinces = { "x10135F" "x4970C4" "x5E30F6" "x657D36" "x7683C4" "x8E1F2E" "x9DA42E" "xC7E891" "xF4E791" }
    traits = {}
    city = "x8e1f2e" #Vul Tenvach
    farm = "xc7e891" #Korrimadhala
    wood = "x9da42e" #Curiyankad
    port = "x5e30f6" #Thekvussam
    mine = "x7683c4" #Uyarbaid
    arable_land = 124
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations bg_dye_plantations bg_tea_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_logging = 9
        bg_fishing = 16
        bg_lead_mining = 27
    }
    naval_exit_id = 3130
}
STATE_ORENKORAIM = {
    id = 727
    subsistence_building = "building_subsistence_farms"
    provinces = { "x078391" "x318DF6" "x3B5225" "x46A12E" "x70A491" "x85E75F" "x889EF6" "x8BB9CF" "x97F646" "xAF2BC4" "xEB98C4" "xEEF45F" "xFF7E32" }
    traits = { state_trait_kalavend_river state_trait_natural_harbors }
    city = "x078391" #Kalavend
    farm = "x318df6" #Cankamam
    wood = "xeb98c4" #Troppaimkq
    port = "xaf2bc4" #Palaya Orenkoraim
    mine = "xeef45f" #Entislula
    arable_land = 186
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations bg_dye_plantations bg_tea_plantations }
    capped_resources = {
        bg_logging = 6
        bg_fishing = 20
    }
    resource = {
        type = "bg_damestear_fields"
        depleted_type = "bg_damestear_mining"
        undiscovered_amount = 3
    }
    naval_exit_id = 3130
}
STATE_EAST_VEYII_SIKARHA = {
    id = 728
    subsistence_building = "building_subsistence_farms"
    provinces = { "x228B71" "x2585B4" "x2657AC" "x36AC26" "x4077F6" "x7C5EB3" "xAC2696" "xAED3B6" "xB36C5E" "xB3B35E" "xDB880C" }
    traits = { state_trait_veyii_sikarha }
    city = "xb36c5e" #RANDOM
    farm = "xac2696" #RANDOM
    wood = "X228b71" #RANDOM
    mine = "X176565" #RANDOM
    arable_land = 60
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_tea_plantations }
    capped_resources = {
        bg_logging = 7
        bg_iron_mining = 21
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 5
    }
}
STATE_KAYDHANO_SEA = {
    id = 729
    subsistence_building = "building_subsistence_farms"
    provinces = { "x296B5A" "x391F1C" "x6B3E29" "x74839B" "x749B7F" "x9B2A1D" "xC78EAD" }
    impassable = { "xc78ead" "x296b5a" "x9b2a1d" "x6b3e29" "x391f1c" "x749b7f" "x74839b" }
    traits = {}
    city = "xc78ead" #RANDOM
    farm = "x296b5a" #RANDOM
    wood = "x9b2a1d" #RANDOM
    mine = "x6b3e29" #RANDOM
    arable_land = 0
}
STATE_PARAIDAR = {
    id = 731
    subsistence_building = "building_subsistence_farms"
    provinces = { "x171FC4" "x80B55F" }
    traits = { state_trait_paradise_island }
    city = "x171FC4" #
    farm = "x171FC4" #
    wood = "x171FC4" #
    port = "x171FC4" #
    #mine = "x738d2e" #Varanyachend
    arable_land = 53
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_dye_plantations bg_tea_plantations bg_cotton_plantations bg_vineyard_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 4
        bg_fishing = 9
        bg_whaling = 3
    }
    naval_exit_id = 3129
}
STATE_WEST_VEYII_SIKARHA = {
    id = 732
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0ECE78" "x176565" "x3F77C4" "x5EB375" "x94853C" "xE70FF6" }
    traits = { state_trait_veyii_sikarha }
    city = "xb36c5e" #RANDOM
    farm = "xac2696" #RANDOM
    wood = "X228b71" #RANDOM
    mine = "X176565" #RANDOM
    arable_land = 53
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_tea_plantations }
    capped_resources = {
        bg_logging = 7
        bg_iron_mining = 24
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 10
    }
}
STATE_URVAND = {
    id = 733
    subsistence_building = "building_subsistence_farms"
    provinces = { "x23557C" "x6A0C5F" "x8282C4" "xA90291" "xBD7991" "xE8C92E" }
    traits = { state_trait_endioka_river }
    city = "x2b33c4" #Enlarmai
    farm = "x43c991" #Sithra Calindash
    wood = "x8282c4" #Imarti Sadvya
    mine = "x4077f6" #Ethra
    arable_land = 100
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_cotton_plantations bg_tobacco_plantations bg_tea_plantations }
    capped_resources = {
        bg_logging = 9
        bg_gold_mining = 2
        bg_lead_mining = 24
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 3
    }
}
STATE_MESOKTI = {
    id = 734
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1A035F" "x1D29F6" "x568D5F" "x5979F6" "x957A91" "x98012E" "xADC75F" "xD4D72E" "xD702C4" }
    traits = { state_trait_kheintroam_river }
    city = "x4c015f" #Varanya Sandaman
    farm = "x957a91" #Dongyccyl
    wood = "x0d1fc4" #Tayakorrim
    port = "x8b0b91" #Korrimutren
    mine = "x80b55f" #Enretvil
    arable_land = 121
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_dye_plantations bg_tea_plantations bg_vineyard_plantations bg_cotton_plantations bg_silk_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 5
        bg_fishing = 5
        bg_sulfur_mining = 24
    }
    naval_exit_id = 3129
}
STATE_SARIHADDU = {
    id = 735
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1A565C" "x2F0988" "x6B1F91" "xC432EC" "xC89A4F" "xEE8C8A" }
    traits = {}
    city = "xa61ff6" #Banderuttai
    farm = "x7c83f6" #Thekavasanikkunnu
    wood = "x6715c4" #Varaendi
    mine = "xd0c75f" #Kattisangamar
    arable_land = 47
    arable_resources = { bg_maize_farms bg_livestock_ranches }
    #capped_resources = {
    #}
    resource = {
        type = "bg_damestear_fields"
        depleted_type = "bg_damestear_mining"
        undiscovered_amount = 3
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 10
    }
}
STATE_CAERGARAEN = {
    id = 736
    subsistence_building = "building_subsistence_farms"
    provinces = { "x05CA2E" "x2C83F6" "x2F0191" "x4401C4" "x8365F6" "xA2F327" "xC2D691" "xE96F5F" "xF2FF2E" }
    traits = { state_trait_kheintroam_river }
    city = "x47c25f" #Nopanais
    farm = "xcb295f" #Budar Gaednae
    wood = "x23dd2e" #Xartraelas
    mine = "x98012e" #Dongaednae
    arable_land = 58
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_tobacco_plantations bg_dye_plantations bg_tea_plantations }
    capped_resources = {
        bg_logging = 4
        bg_sulfur_mining = 16
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 4
    }
}