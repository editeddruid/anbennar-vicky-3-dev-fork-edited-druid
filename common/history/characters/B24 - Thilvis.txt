﻿CHARACTERS = {
	c:B24 ?= {
		create_character = {
			first_name = "Edmund"
			last_name = "Barrows"
			historical = yes
			ruler = yes
			noble = yes
			age = 43
			interest_group = ig_industrialists
			ig_leader = yes
			ideology = ideology_slaver
			traits = {
				cruel experienced_political_operator
			}
		}
		create_character = {
			first_name = "Aldred"
			last_name = sil_Vis
			historical = yes
			noble = yes
			age = 56
			interest_group = ig_landowners
			ig_leader = yes
			ideology = ideology_market_liberal
			traits = {
				expensive_tastes
			}
		}
		create_character = { #Future archprovost of the Trollsbay, in 1850
			first_name = "Cecill"
			last_name = "Locke"
			historical = yes
			age = 36
			interest_group = ig_devout
			ig_leader = yes
			ideology = ideology_moderate
			traits = {
				ambitious
			}
		}
	}
}
