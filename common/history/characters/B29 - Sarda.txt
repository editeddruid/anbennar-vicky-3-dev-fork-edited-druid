﻿CHARACTERS = {
	c:B29 = {
		create_character = {
			first_name = Morvel
			last_name = Vyrekynn
			historical = yes
			ruler = yes
			female = yes
			age = 48
			is_general = yes
			interest_group = ig_landowners
			ideology = ideology_moderate
			traits = {
				brave charismatic celebrity_commander
			}
		}
		create_character = {
			first_name = Adrjon
			last_name = yen_Stanyr
			historical = yes
			age = 45
			ig_leader = yes
			is_general = yes
			interest_group = ig_landowners
			ideology = ideology_moderate
			traits = {
				master_bureaucrat ambitious romantic
			}
		}
		create_character = {
			first_name = Acerad
			last_name = yen_Stenur
			historical = yes
			age = 56
			is_general = yes
			interest_group = ig_landowners
			ideology = ideology_moderate
			traits = {
				reserved forest_commander
			}
		}
		create_character = {
			first_name = Gelinik
			last_name = yen_Polere
			historical = yes
			age = 36
			ig_leader = yes
			is_general = yes
			interest_group = ig_armed_forces
			ideology = ideology_royalist
			traits = {
				forest_commander honorable
			}
		}
		create_character = { #grand priest of Adbrabohvi
			first_name = Devynn
			last_name = yen_Bohvi
			historical = yes
			age = 62
			ig_leader = yes
			interest_group = ig_devout
			ideology = ideology_traditionalist
			traits = {
				imperious innovative
			}
		}
	}
}
