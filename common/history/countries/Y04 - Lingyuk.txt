﻿COUNTRIES = {
	c:Y04 = {
		effect_starting_technology_tier_4_tech = yes
		add_technology_researched = urban_planning
		add_technology_researched = sericulture
		add_technology_researched = academia
		add_technology_researched = law_enforcement
		
		effect_starting_politics_traditional = yes
		activate_law = law_type:law_theocracy
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_national_militia

		activate_law = law_type:law_isolationism
		activate_law = law_type:law_tenant_farmers

		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_women_own_property # Not allowed women in workplace without voting
		activate_law = law_type:law_migration_controls
		activate_law = law_type:law_legacy_slavery
	}
}