﻿positivism_inventor_template = {
	first_name = Wishus
	last_name = Piggus
	historical = yes
	birth_date = 1798.1.19
	culture = cu:testorian
	religion = rel:atheist
	is_agitator = yes
	female = no
	interest_group = ig_intelligentsia
	ideology = ideology_positivist
	#dna = 
	traits = {
		charismatic
		sickly
		erudite
	}
}
